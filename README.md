![tailscale logo](https://i.imgur.com/Aq7XdAB.png)

# Tailscale Docker-based Relay

### Overview 

[[_TOC_]]

## Quickstart

-----------------------------------

```sh
# amd64 architecture
docker pull registry.gitlab.com/gruberx/tailscale-docker:latest
# renaming the image
docker tag registry.gitlab.com/gruberx/tailscale-docker:latest tailscale:latest
# running a very basic setup, most features won't work but you can test your connection 
docker run tailscale:latest --cap-add=NET_ADMIN 
# or docker run tailscale:{VERSION} --capp-add=NET_ADMIN
```

# In-depth deployment

------------------------------------------------

This is a docker image based on `alpine:3.12` for setting up a [tailscale](https://tailscale.com) instance in relay mode.

## Prerequisites

- Defined docker network via `docker network create -d bridge <network name>`
- Subnet network via `docker inspect <NETWORK ID> | grep Subnet`
- Auth key from https://login.tailscale.com/admin/authkeys (e.g. `tskey-123abc...`)

### Optional

- docker-commpose for persistent setup

## Requirements

- `--cap-add=NET_ADMIN` [Reference .1](https://docs.docker.com/engine/reference/run/#runtime-privilege-and-linux-capabilities)
- `--sysctl net.ipv4.ip_forward=1` [Reference .3](http://docs.docker.oeynet.com/engine/userguide/networking/default_network/container-communication/#communicating-to-the-outside-world)
- Volume for persistent storage `/tailscale_data`

## Run using Docker as a Container

Run the following `docker run` command.

- `AUTHKEY=tskey-123abc...`
- `ROUTES=172.31.0.0/16`

```bash
cd tailscale \
    && docker run -d \
       -v /tailscale_data \
       --cap-add=NET_ADMIN \
       --network=<docker_net> \
       # Or you can also use --network host and use your actual subnet local ipv4 addresses \
       -e "ROUTES=<docker_network>" \ 
       -e "AUTHKEY=<your_auth_key>" \ 
       registry.gitlab.com/gruberx/tailscale-docker:latest
```

## Running on Docker-compose for persistence 

```bash
version: "3.8"
services:
  tailscaled:
    container_name: tailscaled
    cap_add:
      - NET_ADMIN
    networks:
      - <your_networks>
    env_file:
      - .env.example
    build:
      context: ./tailscale
    volumes:
      - tailscale_data:/var/lib/tailscale
    sysctls:
      - net.ipv4.conf.all.src_valid_mark=1
    restart: unless-stopped
volumes:
  tailscale_data:
```

Find this image on [Gitlab container registry](https://gitlab.com/gruberx/tailscale-docker/container_registry/1639474)
It is available on the following architectures:
- [x86/amd64](registry.gitlab.com/gruberx/tailscale-docker:latest)
- [arm/arm64](registry.gitlab.com/gruberx/tailscale-docker:raspb-pi)
